import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { RedisClient } from './utils/redis';
async function bootstrap() {
  RedisClient.getClient();

  const app = await NestFactory.create(AppModule);
  await app.listen(3000);
}
bootstrap();
