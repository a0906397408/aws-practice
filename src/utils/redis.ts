// import { RedisClientType } from '@node-redis/client';
import { createClient, RedisClientType } from 'redis';
export class RedisClient {
  private static instance: RedisClientType;

  public static async getClient() {
    console.log('Redis Client load...');
    if (!RedisClient.instance) {
      RedisClient.instance = createClient({
        url: 'redis://default:default@35.77.11.164:6379',
      });
      RedisClient.instance.on('error', (err) =>
        console.log('Redis Client Error', err),
      );
      await RedisClient.instance.connect();
      RedisClient.instance.set('key3', '1231312312');
      console.log(await RedisClient.instance.get('key2'));
      console.log('Redis Client Success');

      // console.log(connect);
    }

    return RedisClient.instance;
  }
}
